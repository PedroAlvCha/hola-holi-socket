import socketIOClient from 'socket.io-client'
import * as Sentry from '@sentry/browser';

const apiEndpoint =  'http://localhost:9001';
const mySocket = socketIOClient(apiEndpoint);

Sentry.init({
 dsn: "https://b4f74186281c4d06a97bf1b24e8da00a@sentry.io/1365992"
});

export function emitGreetingSocket (greeting){
  console.log('emitGreetingSocket got called');
  try{
	console.log('The value for greeting is:', greeting);
	mySocket.emit('greeting', 'hola');
  } catch (e) {
	  let myErrorE = e;
	  myErrorE.position = 'emitGreetingSocket';
	  console.log('We have an error in emitGreetingSocket:', myErrorE);
	  Sentry.captureException(myErrorE);
  }
}