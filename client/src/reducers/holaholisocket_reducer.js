import * as Sentry from '@sentry/browser';
import  {
          CHANGE_HOLA_HOLI,
		  SET_ERROR, 
		  CLEAR_ERROR,
        } from '../actions/holaholisocket_actions.js'

Sentry.init({
 dsn: "https://b4f74186281c4d06a97bf1b24e8da00a@sentry.io/1365992"
});

const initialGreetingState = {
	greeting: 'goodbye',
	error: null,
	socketEndpoint: "http://localhost:9001",
}

export function holaHoliSocketManager (state = initialGreetingState, action) {
  try{
	  switch (action.type) {
		case CHANGE_HOLA_HOLI :
		  const localGreetingReceived = action.payload
		  return {
			  ...state,
			  greeting:localGreetingReceived,
		  }
		case SET_ERROR :
		  const myError = action.payload;
		  console.log('We are at SET_ERROR reducer with error: ',myError);
		  return {
			...state,
			error:myError,
		  }
		case CLEAR_ERROR :
		  return {
			...state,
			error:null,
		  }
		default :
		  return state
	  }
  } catch (e) {
    let myErrorE = e;
	myErrorE.position = 'holaHoliSocketManager';
	console.log('We have an error in holaHoliSocketManager:', myErrorE);
	Sentry.captureException(myErrorE);
  }
}
