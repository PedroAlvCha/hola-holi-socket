import { combineReducers } from 'redux';
import { holaHoliSocketManager } from './holaholisocket_reducer.js';

const rootReducer = combineReducers({
  holaHoliSocketManager,
});

export {rootReducer};
