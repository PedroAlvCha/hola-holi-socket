import * as contentAPIutil from '../utils/contentAPI.js';
import * as Sentry from '@sentry/browser';


export const CHANGE_HOLA_HOLI = 'SEARCH_LIST_CHANGE_SORT_ASCDESC';
export const SET_ERROR = 'SET_ERROR';
export const CLEAR_ERROR = 'CLEAR_ERROR';


Sentry.init({
 dsn: "https://b4f74186281c4d06a97bf1b24e8da00a@sentry.io/1365992"
});

export function sendGreetingSocket(greeting){
  try{
	  console.log('sendGreetingSocket got called:');
	  console.dir(greeting);
	  console.log('after console dir');
	  contentAPIutil.emitGreetingSocket(greeting);
	  return {
		type: CHANGE_HOLA_HOLI,
		greeting,
	  }
  } catch (e) {
    let myErrorE = e;
	myErrorE.position = 'sendGreetingSocket';
	console.log('We have an error in sendGreetingSocket:', myErrorE);
	Sentry.captureException(myErrorE);
  }
}

export function clearError(){
  console.log('clearError got called');
  return {
    type: CLEAR_ERROR,
  }
};

export function setError( error ){
  console.log('setError got called', error);
  return {
    type: SET_ERROR,
	error, 
  }
};
