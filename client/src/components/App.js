import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';
import HolaHoliSocketComponent  from './HolaHoliSocket.js';
import * as Sentry from '@sentry/browser';
import {  Button } from 'react-bootstrap';
var util = require('util');

Sentry.init({
 dsn: "https://b4f74186281c4d06a97bf1b24e8da00a@sentry.io/1365992"
});

class App extends Component {

    componentDidCatch(error, errorInfo) {
      Sentry.withScope(scope => {
        Object.keys(errorInfo).forEach(key => {
          scope.setExtra(key, errorInfo[key]);
        });
		console.log('We caught an error at APP:', util.inspect(error));
        Sentry.captureException(error);
      });
    }
    render() {
		const {   myLocalError  } = this.props;
		if(myLocalError){
          return (
              <Button onClick={() => Sentry.showReportDialog()}>Report feedback</Button>
          );
		} else {
		  return (
			<div>
			  <HolaHoliSocketComponent />
			</div>
		  );		
		}
	}
}

function mapStateToProps (state) {
  return {
	  myLocalError:state.holaHoliSocketManager.error,
	  mySocketEndpoint:state.holaHoliSocketManager.socketEndpoint,
  }
}

function mapDispatchToProps (dispatch) {
  return {
  }
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

