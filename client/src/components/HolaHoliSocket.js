import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import * as Sentry from '@sentry/browser';
import {  Button } from 'react-bootstrap';
import { sendGreetingSocket } from '../actions/holaholisocket_actions.js';


class HolaHoliSocketComponent extends Component {

  componentDidCatch(error, errorInfo) {
    Sentry.withScope(scope => {
      Object.keys(errorInfo).forEach(key => {
        scope.setExtra(key, errorInfo[key]);
      });
	  console.log('We caught an error at UserSearch:', error);
      Sentry.captureException(error);
    });
  }

  render(){
    const {  myLocalError
			, mySendGreetingSocket
			, myLocalGreeting} = this.props;
    if(myLocalError){
      return (
        <Button onClick={() => Sentry.showReportDialog()}>Report feedback</Button>
      );
	} else {
	  return(
          <div>
			<Button onClick={mySendGreetingSocket('hola')}>SendGreeting</Button>
          </div>
      )
	}
  }
};

function mapStateToProps (state, ownProps) {
  return {
	  myLocalError:state.holaHoliSocketManager.error,
	  myLocalGreeting:state.holaHoliSocketManager.greeting,
  }
};

function mapDispatchToProps (dispatch) {
  return {
	  mySendGreetingSocket:(data) =>dispatch(sendGreetingSocket(data)),
  }
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HolaHoliSocketComponent);
