//Debugging Variables
const appName = 'holaholi.controller';
const Mydebbuger = require('debug')(appName);



//declare array to store multiple users


const getHolaHoli = (req, res) => {
  let socket_id = [];
  Mydebbuger('Calling getHolaHoli');
  try {
   const io = req.app.get('socketio');
   io.on('connection', socket => {
      socket_id.push(socket.id);
      if (socket_id[0] === socket.id) {
        // remove the connection listener for any subsequent 
        // connections with the same ID
        io.removeAllListeners('connection'); 
      }

      socket.on('get', msg => {
        Mydebbuger('just gotGet: ', msg);
        socket.emit('HOLA', 'Hola from server');
      })

   });
  } catch (e) {
    Mydebbuger('error obtained in the getHolaHoli: ', e);
  }
}


const sendGreeting = (req, res) => {
  let socket_id = [];
  Mydebbuger('Calling getHolaHoli');
  try {
   const io = req.app.get('socketio');
   io.on('connection', socket => {
      socket_id.push(socket.id);
      if (socket_id[0] === socket.id) {
        // remove the connection listener for any subsequent 
        // connections with the same ID
        io.removeAllListeners('connection'); 
      }

      socket.on('hello message', msg => {
        Mydebbuger('just got: ', msg);
        socket.emit('greeting', 'holi from server');
      })

   });
  } catch (e) {
    Mydebbuger('error obtained in the uploadCsvFile: ', e);
  }
}


const postNotRouteValid = (req, res) => {
  Mydebbuger('Calling postNotRouteValid');
  try {
    const myResponse = "Invalid Route, does not Exist."
    res.send(myResponse);
  } catch (e) {
    Mydebbuger('Error obtained in the postNotRouteValid: ', e);
  }
}

const getNotRouteValid = (req, res) => {
  Mydebbuger('Calling getNotRouteValid');
  try {
    const myResponse = "Invalid Route, does not Exist."
    res.send(myResponse);
  } catch (e) {
    Mydebbuger('Error obtained in the getNotRouteValid: ', e);
  }
}

module.exports = {
  getHolaHoli,
  sendGreeting,
  getNotRouteValid,
  postNotRouteValid,
}
