const appName = 'holaholisocket.routes';
const Mydebbuger = require('debug')(appName);
const holaHoliSocketController = require('../controllers/HolaHoliController');

module.exports = (router) => {
  try {
    router.use((req, res, next) => {
	  Mydebbuger('Router is in use, got called with this URL: ', req.url, ' and request method', req.method);
      next();
    });
  } catch (e) {
    Mydebbuger('Error in router creation: ', e);
  }

    // routes
  try {
    router.get('/', holaHoliSocketController.getCsvParser);
    router.post('/greeting', holaHoliSocketController.sendGreeting);
    router.get('*', holaHoliSocketController.getNotRouteValid);
    router.post('*', holaHoliSocketController.postNotRouteValid); 
  } catch (e) {
    Mydebbuger('Error in router route assignments: ', e);
  }
};