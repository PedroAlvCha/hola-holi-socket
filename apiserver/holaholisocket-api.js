require('dotenv').config()
const appName = 'holaholisocket.api';
const app = require('express')();
const Mydebbuger = require('debug')(appName);
const Sentry = require('@sentry/node');
const port = process.env.HOLAHOLI_API_PORT || process.env.SETUP_HOLAHOLI_API_PORT; //obtain the PORT for the service
const routes = require('./routes/routes');
const bodyParser = require('body-parser');


Sentry.init({ dsn: process.env.sentry_csvparser_node });


var server = app.listen(port);
var io = require('socket.io')(server);

app.set('socketio', io);

Mydebbuger('Adding middlewares to our server');

try{
  // midlewares
  app.use((req, res, next) => {
    Mydebbuger('App is in use, got called with this URL: ', req.url, ' and request method', req.method);
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	res.header('Access-Control-Request-Method', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
  });
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
} catch (e2) {
  Mydebbuger('Error adding middlewares for server creations: ', e2);
}
// routes
try {
  Mydebbuger('We are now assigning route handling.');
  routes(app);
} catch (e) {
  Mydebbuger('Error in route assignment for server creations: ', e);
}

module.exports = server;